import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UsuariosListComponent} from '../usuarios/containers/usuarios-list/usuarios-list.component';


const routes: Routes = [
  {
    path: '',
    component: UsuariosListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule {
}
