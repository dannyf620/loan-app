import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuariosListComponent } from './containers/usuarios-list/usuarios-list.component';
import {UsuariosRoutingModule} from './usuarios-routing.module';

@NgModule({
  declarations: [UsuariosListComponent],
  imports: [
    CommonModule,
    UsuariosRoutingModule
  ]
})
export class UsuariosModule { }
