



  export interface DocType {
    id: number;
    name: string;
  }

  export interface User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    documentType: number;
    documentNumber: string;
    phoneNumber: string;
    email: string;
    userState: number;
    loans: number[];
    roles: string[];
  }

  export interface UserState {
    id: number;
    name: string;
    lowerLimit: number;
    upperLimit: number;
  }


