import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarPrestamoComponent } from './solicitar-prestamo.component';

describe('SolicitarPrestamoComponent', () => {
  let component: SolicitarPrestamoComponent;
  let fixture: ComponentFixture<SolicitarPrestamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitarPrestamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarPrestamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
