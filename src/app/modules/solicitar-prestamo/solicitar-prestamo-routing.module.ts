import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SolicitarPrestamoComponent} from './containers/solicitar-prestamo/solicitar-prestamo.component';


const routes: Routes = [
  {
    path: '',
    component: SolicitarPrestamoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitarPrestamoRoutingModule {
}
