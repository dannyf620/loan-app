export interface Loan {
  id: number;
  startDate: Date;
  endDate: Date;
  whoApproved: number[];
  loanAmount: number;
  currentAmount: number;
  loanState: number;
  completed: boolean;
  duration: string;
  annualRate: number;
  user: number;
}

export interface LoanState {
  id: number;
  name: string;
  description: string;
}

export interface Transaction {
  id: number;
  loan: number;
  date: Date;
  type: string;
  amount: number;
  user: number;
  description: string;
}
