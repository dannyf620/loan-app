import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SolicitarPrestamoComponent} from './containers/solicitar-prestamo/solicitar-prestamo.component';
import {SolicitarPrestamoRoutingModule} from './solicitar-prestamo-routing.module';

@NgModule({
  declarations: [SolicitarPrestamoComponent],
  imports: [
    CommonModule,
    SolicitarPrestamoRoutingModule
  ]
})
export class SolicitarPrestamoModule {
}
