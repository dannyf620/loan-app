import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SolicitudesListComponent } from './containers/solicitudes-list/solicitudes-list.component';
import {SolicitudesRoutingModule} from './solicitudes-routing.module';

@NgModule({
  declarations: [SolicitudesListComponent],
  imports: [
    CommonModule,
    SolicitudesRoutingModule
  ]
})
export class SolicitudesModule { }
