import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SolicitudesListComponent} from './containers/solicitudes-list/solicitudes-list.component';


const routes: Routes = [
  {
    path: '',
    component: SolicitudesListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitudesRoutingModule {
}
