import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {BancoComponent} from './containers/banco/banco.component';
import {MainContainerComponent} from './containers/main-container/main-container.component';
import {SidebarContainerComponent} from './containers/sidebar-container/sidebar-container.component';


const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../dashboard/dashboard.module').then(mod => mod.DashboardModule)
  }, {
    path: 'prestamo',
    loadChildren: () => import('../solicitar-prestamo/solicitar-prestamo.module').then(mod => mod.SolicitarPrestamoModule)
  }, {
    path: 'solicitudes',
    loadChildren: () => import('../solicitudes/solicitudes.module').then(mod => mod.SolicitudesModule)
  }, {
    path: 'usuarios',
    loadChildren: () => import('../usuarios/usuarios.module').then(mod => mod.UsuariosModule)
  }, {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {
}
