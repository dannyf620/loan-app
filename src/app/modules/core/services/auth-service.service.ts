import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../../usuarios/models/user';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  apiURL = environment.apiURL;

  constructor(private httpClient: HttpClient) {
  }

  public login(user: { username: string, password: string }): Observable<User> {
    const query = `?username=${user.username}&password=${user.password}`;
    console.log(this.apiURL + 'api/users' + query);
    return this.httpClient.get<User>(this.apiURL + 'api/users' + query)
      .pipe(
        map(userResponse => {
          return userResponse && userResponse[0];
        })
      );
  }

  public register(user: User): Observable<User> {
    return this.httpClient.post<User>(this.apiURL, user);
  }

  private getUser(idUser: number): Observable<{ id: number }> {
    return this.httpClient.post<{ id: number }>(this.apiURL, idUser);
  }
}
