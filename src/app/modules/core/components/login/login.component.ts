import {Component, OnInit} from '@angular/core';
import {User} from '../../../usuarios/models/user';
import {AuthServiceService} from '../../services/auth-service.service';
import {MatDialogRef} from '@angular/material';
import {Login} from '../../store/auth/auth.actions';
import {Store} from '@ngrx/store';
import {AuthState} from '../../store/auth/auth.reducer';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: User = {
    id: 1,
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    documentType: 1,
    documentNumber: '',
    phoneNumber: '',
    email: '',
    userState: 1,
    loans: [],
    roles: ['User']
  };
  errMess: string;

  constructor(
    public dialogRef: MatDialogRef<LoginComponent>,
    private authService: AuthServiceService,
    private router: Router,
    private store: Store<AuthState>) {
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log('User: ', this.user);
    this.authService.login(this.user)
      .subscribe(res => {
          if (res && res.id) {
            this.dialogRef.close(res);
            this.store.dispatch(new Login({user: res}));
            this.router.navigateByUrl('/dashboard');
          } else {
            this.dialogRef.close(res);
          }
        },
        error => {
          console.log(error);
          this.errMess = error;
        });
  }
}
