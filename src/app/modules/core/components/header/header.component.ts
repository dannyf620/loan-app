import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../usuarios/models/user';
import {MatDialog} from '@angular/material/dialog';
import {LoginComponent} from '../login/login.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() user: User;
  @Output() openMenuEmitter = new EventEmitter();

  constructor(public router: Router) {
  }

  ngOnInit() {
    console.log(this.user);
  }

  navigate(page) {
    this.router.navigate([page]);
  }

  openMenu() {
    this.openMenuEmitter.emit('');
  }
}
