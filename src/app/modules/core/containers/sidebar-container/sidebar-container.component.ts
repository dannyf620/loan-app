import {Component, OnInit, ViewChild} from '@angular/core';
// import {MatSidenav} from '@angular/material';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {User} from '../../../usuarios/models/user';
import {CoreState} from '../../store/reducers';
import * as authSelectors from '../../store/auth/auth.selectors';
@Component({
  selector: 'app-sidebar-container',
  templateUrl: './sidebar-container.component.html',
  styleUrls: ['./sidebar-container.component.scss']
})
export class SidebarContainerComponent implements OnInit {
  user$: Observable<User>;
  // @ViewChild('sidenav') sidenav: MatSidenav;
  // navMode = 'side';

  constructor(private store: Store<CoreState>) {
    this.user$ = store.select(authSelectors.currentUser);
  }

  ngOnInit() {
  }

}
