import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';

import * as fromAuth from './auth/auth.reducer';

export interface CoreState {
  auth: fromAuth.AuthState;
}

export const reducers: ActionReducerMap<CoreState> = {
  auth: fromAuth.reducer
};

export const getInboxState = createFeatureSelector<CoreState>(
  'core'
);
