import {Actions, Effect, ofType} from '@ngrx/effects';
import {defer, Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {tap} from 'rxjs/operators';
import {AuthActionTypes, Login, Logout} from './auth.actions';


@Injectable()
export class AuthEffects {
  @Effect({dispatch: false})
  login$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.loginAction),
    tap(action => {
      localStorage.setItem('user', JSON.stringify(action.payload.user));
      console.log('----> effect');
    })
  );

  @Effect({dispatch: false})
  logout$ = this.actions$.pipe(
    ofType<Logout>(AuthActionTypes.LogoutAction),
    tap(() => {
      localStorage.removeItem('user');
      this.router.navigateByUrl('/');
    })
  );

  @Effect({})
  init$ = defer((): Observable<Login | Logout> => {
    const userData = localStorage.getItem('user');
    if (userData && userData !== 'undefined') {
      return of(new Login(JSON.parse(userData)));
    } else {
      return of(new Logout());
    }
  });

  constructor(private actions$: Actions, private router: Router) {
  }

}
