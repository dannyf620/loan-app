import { Action } from '@ngrx/store';
import {User} from '../../../usuarios/models/user';

export enum AuthActionTypes {
  loginAction = '[Auth] Load Auths',
  LogoutAction = '[Logout] Action'
}

export class Login implements Action {
  readonly type = AuthActionTypes.loginAction;
  constructor(public payload: { user: User }) {
    console.log('----> actins');
  }
}

export class Logout implements Action {
  readonly type = AuthActionTypes.LogoutAction;
}
export type AuthActions = Login| Logout;
