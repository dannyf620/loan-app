import {AuthActions, AuthActionTypes} from './auth.actions';
import {User} from '../../../usuarios/models/user';

export interface AuthState {
  loggedIn: boolean;
  user: User;
}

export const initialAuthState: AuthState = {
  loggedIn: false,
  user: undefined
};

export function reducer(state = initialAuthState, action: AuthActions): AuthState {
  switch (action.type) {

    case AuthActionTypes.loginAction:
      return {...state, loggedIn: true, user: action.payload.user};
    case AuthActionTypes.LogoutAction:
      return {
        loggedIn: false,
        user: undefined
      };
    default:
      return state;
  }
}
