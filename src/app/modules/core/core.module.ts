import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainContainerComponent} from './containers/main-container/main-container.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {MaterialModule} from '../material/material.module';
import {SidebarContainerComponent} from './containers/sidebar-container/sidebar-container.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {BancoComponent} from './containers/banco/banco.component';
import {HeaderComponent} from './components/header/header.component';
import {MatDialogModule} from '@angular/material/dialog';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import {reducers} from './store/reducers';
import {AuthEffects} from './store/auth/auth.effects';
import {EffectsModule} from '@ngrx/effects';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import { SidebarContentComponent } from './components/sidebar-content/sidebar-content.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    MainContainerComponent,
    LoginComponent,
    RegisterComponent,
    SidebarContainerComponent,
    NotFoundComponent,
    BancoComponent,
    HeaderComponent,
    SidebarComponent,
    SidebarContentComponent,
    SideMenuComponent
  ],
  exports: [
    MainContainerComponent,
    SidebarComponent,
    SidebarContentComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    MatDialogModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forFeature('auth', reducers),
    EffectsModule.forFeature([AuthEffects]),
    FlexLayoutModule,
  ], entryComponents: [
    LoginComponent, HeaderComponent
  ]
})
export class CoreModule {
}
